#### Wprowadzenie
Zadaniem aplikacji jest obliczanie miesięcznego wynagrodzenia netto na podstawie dziennego wynagrodzenia brutto.

Użytkownik podaje kraj w którym uzyskał przychód w celu uwzględnienia:

   * waluty
   * podatku dochodowego
   * kosztów stałych

#### Uruchomienie
Do zbudowania oraz uruchomienia projektu potrzebny jest Maven.

Budowanie:

    mvn package

Uruchomienie:

    mvn spring-boot:run

Wywołanie:

    http://localhost:8080

#### Dodawanie nowych państw
Nowe państwa można dodawać w pliku:

    src/main/resources/data.sql

Państwa zostaną ponownie załadowane po ponownym uruchomieniu.