package pl.wojt.salarycalculator.model;

import lombok.Data;

@Data
public class Calculation {
    private Integer selectedCountryId;
    private Country country;
    private Float exchangeRate;
    private Float grossAmountDaily;
    private Float netAmountMonthly;
}
