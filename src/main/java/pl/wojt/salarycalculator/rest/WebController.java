package pl.wojt.salarycalculator.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import pl.wojt.salarycalculator.model.Calculation;
import pl.wojt.salarycalculator.model.Country;
import pl.wojt.salarycalculator.repository.CountryRepository;
import pl.wojt.salarycalculator.service.CurrencyService;
import pl.wojt.salarycalculator.service.SalaryService;

import java.time.Duration;
import java.util.Optional;

@Controller
public class WebController {

    @Autowired
    protected CountryRepository countryRepository;

    @Autowired
    protected CurrencyService currencyService;

    @Autowired
    protected SalaryService salaryService;

    @GetMapping("/")
    public String calculateForm(Model model) {
        model.addAttribute("calculation", new Calculation());
        model.addAttribute("allCountries", countryRepository.findAll());
        return "form";
    }

    @PostMapping("/result")
    public String calculateResult(@ModelAttribute("calculation") Calculation calculation) throws Exception {

        Float grossAmountDaily = Optional.ofNullable(calculation.getGrossAmountDaily())
                .filter(amount -> amount >= 0.0f)
                .orElseThrow(() -> new Exception("niepoprawna kwota"));

        Country country = Optional.ofNullable(calculation.getSelectedCountryId())
                .flatMap(id -> countryRepository.findById(id))
                .orElseThrow(() -> new Exception("niepoprawny kraj"));

        Float exchangeRate = Optional.ofNullable(country.getCurrency())
                .map(currency -> currencyService.getExchangeRate(currency))
                .orElseThrow(() -> new Exception("nie można pobrać kursów walut"));

        float grossAmount = salaryService.calculateMonthlySalary(grossAmountDaily);
        float exchangedGrossAmount = exchangeRate * grossAmount;

        Float netAmountMonthly = Optional.ofNullable(
                    salaryService.calculateNetSalary(
                        exchangedGrossAmount,
                        country.getFixedCost(),
                        country.getIncomeTax())
                ).orElseThrow(() -> new Exception("błąd obliczeń"));

        calculation.setCountry(country);
        calculation.setExchangeRate(exchangeRate);
        calculation.setNetAmountMonthly(netAmountMonthly);

        return "result";
    }

    @ExceptionHandler({Exception.class})
    public String error(Exception e, Model model) {
        model.addAttribute("message", e.getMessage());
        return "error";
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder
                .setConnectTimeout(Duration.ofMillis(500))
                .setReadTimeout(Duration.ofMillis(500))
                .build();
    }

}
