package pl.wojt.salarycalculator.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wojt.salarycalculator.model.Country;

@Repository
public interface CountryRepository extends CrudRepository<Country, Integer> {
}
