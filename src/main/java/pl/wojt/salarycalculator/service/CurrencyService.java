package pl.wojt.salarycalculator.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class CurrencyService {

    protected final String URL = "http://api.nbp.pl/api/exchangerates/rates/a/{0}";

    protected Logger logger = LoggerFactory.getLogger(CurrencyService.class);

    @Autowired
    protected RestTemplate restTemplate;

    public Float getExchangeRate(String currencyCode) {

        if (currencyCode == null) {
            return null;
        }

        if (currencyCode.equals("PLN")) {
            return 1.0f;
        }

        Float rate;

        try {
            NbpApiResponse response = restTemplate.getForObject(URL, NbpApiResponse.class, currencyCode);
            rate = response.rates.get(0).mid;
        } catch (Exception e) {
            logger.error("External REST error: " + e.getMessage());
            rate = null;
        }

        return rate;
    }

    public static class NbpApiResponse {
        public String table;
        public String currency;
        public String code;
        public List<NbpApiRate> rates = new ArrayList<>();
    }

    public static class NbpApiRate {
        public String no;
        public String effectiveDate;
        public Float mid;
    }

}
