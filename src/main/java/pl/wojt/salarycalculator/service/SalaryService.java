package pl.wojt.salarycalculator.service;

import org.springframework.stereotype.Service;

@Service
public class SalaryService {

    private static final int WORK_DAYS_IN_MONTH = 22;

    public Float calculateMonthlySalary(float dailyAmount) {
        return dailyAmount * WORK_DAYS_IN_MONTH;
    }

    public Float calculateNetSalary(float grossAmount, float fixedCost, float incomeTax) {
        float taxedAmount = grossAmount > fixedCost ? grossAmount - fixedCost : 0;
        float taxAmount = incomeTax * taxedAmount;
        float netSalary = grossAmount - taxAmount;
        return Float.isFinite(netSalary) ? netSalary : null;
    }

}
