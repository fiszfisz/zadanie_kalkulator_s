package pl.wojt.salarycalculator.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.wojt.salarycalculator.model.Calculation;
import pl.wojt.salarycalculator.model.Country;
import pl.wojt.salarycalculator.repository.CountryRepository;
import pl.wojt.salarycalculator.service.CurrencyService;
import pl.wojt.salarycalculator.service.SalaryService;

import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WebControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CountryRepository countryRepository;

    @MockBean
    private CurrencyService currencyService;

    @MockBean
    private SalaryService salaryService;

    private Calculation calculation;
    private Country country;

    @Before
    public void setup() {
        calculation = new Calculation();
        calculation.setSelectedCountryId(1);
        calculation.setGrossAmountDaily(100.0f);

        country = new Country();
        country.setId(1);
        country.setCode("TST");
        country.setName("Test Country");
        country.setCurrency("CUR");
        country.setIncomeTax(0.1f);
        country.setFixedCost(100);
    }

    @Test
    public void calculateForm_success() throws Exception {
        when(countryRepository.findAll()).thenReturn(Collections.singletonList(country));

        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("form"))
                .andExpect(model().attributeExists("calculation"))
                .andExpect(model().attribute("allCountries", hasItem(
                        allOf(
                                hasProperty("id", is(1)),
                                hasProperty("name", is("Test Country")),
                                hasProperty("currency", is("CUR"))
                        )
                )));
    }

    @Test
    public void calculateResult_success() throws Exception {
        when(countryRepository.findById(1)).thenReturn(Optional.of(country));
        when(currencyService.getExchangeRate("CUR")).thenReturn(1.0f);
        when(salaryService.calculateMonthlySalary(100.f)).thenReturn(2000.0f);
        when(salaryService.calculateNetSalary(2000.0f, 100, 0.1f)).thenReturn(1000.0f);

        mockMvc.perform(post("/result")
                .flashAttr("calculation", calculation))
                .andExpect(status().isOk())
                .andExpect(view().name("result"))
                .andExpect(model().attribute("calculation", allOf(
                        hasProperty("country", allOf(
                                hasProperty("id", is(1)),
                                hasProperty("name", is("Test Country")),
                                hasProperty("currency", is("CUR"))
                        )),
                        hasProperty("exchangeRate", is(1.0f)),
                        hasProperty("grossAmountDaily", is(100.0f)),
                        hasProperty("netAmountMonthly", is(1000.0f))
                )));
    }

    @Test
    public void calculateResult_noCountrySelected() throws Exception {
        calculation.setSelectedCountryId(null);

        mockMvc.perform(post("/result")
                .flashAttr("calculation", calculation))
                .andExpect(status().isOk())
                .andExpect(view().name("error"))
                .andExpect(model().attributeExists("message"));
    }

    @Test
    public void calculateResult_noGrossAmountSelected() throws Exception {
        calculation.setGrossAmountDaily(null);

        mockMvc.perform(post("/result")
                .flashAttr("calculation", calculation))
                .andExpect(status().isOk())
                .andExpect(view().name("error"))
                .andExpect(model().attributeExists("message"));
    }

    @Test
    public void calculateResult_negativeGrossAmountSelected() throws Exception {
        calculation.setGrossAmountDaily(-100.0f);

        mockMvc.perform(post("/result")
                .flashAttr("calculation", calculation))
                .andExpect(status().isOk())
                .andExpect(view().name("error"))
                .andExpect(model().attributeExists("message"));
    }

    @Test
    public void calculateResult_currencyServiceError() throws Exception {
        when(countryRepository.findById(1)).thenReturn(Optional.of(country));
        when(currencyService.getExchangeRate("CUR")).thenReturn(null);

        mockMvc.perform(post("/result")
                .flashAttr("calculation", calculation))
                .andExpect(status().isOk())
                .andExpect(view().name("error"))
                .andExpect(model().attributeExists("message"));
    }

    @Test
    public void calculateResult_salaryServiceError() throws Exception {
        when(countryRepository.findById(1)).thenReturn(Optional.of(country));
        when(currencyService.getExchangeRate("CUR")).thenReturn(1.0f);
        when(salaryService.calculateMonthlySalary(100.f)).thenReturn(2000.0f);
        when(salaryService.calculateNetSalary(2000.f, 100, 0.1f)).thenReturn(null);

        mockMvc.perform(post("/result")
                .flashAttr("calculation", calculation))
                .andExpect(status().isOk())
                .andExpect(view().name("error"))
                .andExpect(model().attributeExists("message"));
    }

}