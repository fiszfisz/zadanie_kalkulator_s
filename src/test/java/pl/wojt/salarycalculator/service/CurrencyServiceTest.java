package pl.wojt.salarycalculator.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CurrencyServiceTest {

    private static final float DELTA = 0.001f;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private CurrencyService currencyService;

    private String testCurrencyCode;
    private CurrencyService.NbpApiResponse testResponse;

    @Before
    public void init() {
        testResponse = new CurrencyService.NbpApiResponse();
        testResponse.rates.add(new CurrencyService.NbpApiRate());
    }

    @Test
    public void getExchangeRate_success() {
        testCurrencyCode = "XYZ";
        testResponse.rates.get(0).mid = 3.14f;

        when(restTemplate.getForObject(currencyService.URL, CurrencyService.NbpApiResponse.class, testCurrencyCode))
                .thenReturn(testResponse);

        assertEquals(3.14f, currencyService.getExchangeRate(testCurrencyCode), DELTA);
    }

    @Test
    public void getExchangeRate_successPln() {
        testCurrencyCode = "PLN";
        assertEquals(1.0f, currencyService.getExchangeRate(testCurrencyCode), DELTA);
    }

    @Test
    public void getExchangeRate_restRequestException() {
        testCurrencyCode = "XYZ";
        testResponse.rates.get(0).mid = 3.14f;

        when(restTemplate.getForObject(currencyService.URL, CurrencyService.NbpApiResponse.class, testCurrencyCode))
                .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));

        assertNull(currencyService.getExchangeRate(testCurrencyCode));
    }

    @Test
    public void getExchangeRate_nullParameter() {
        assertNull(currencyService.getExchangeRate(null));
    }

}