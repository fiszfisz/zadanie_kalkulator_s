package pl.wojt.salarycalculator.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SalaryServiceTest {

    private static final float DELTA = 0.001f;

    @Autowired
    private SalaryService salaryService;

    private Float result;

    @Test
    public void calculateMonthlySalary_correctCalculation() {
        result = salaryService.calculateMonthlySalary(100.0f);
        assertEquals(2200.0f, result, DELTA);
    }

    @Test
    public void calculateNetSalary_grossAmountGreaterThanFixedCost() {
        result = salaryService.calculateNetSalary(300.0f, 200.0f, 0.5f);
        assertEquals(250.0f, result, DELTA);
    }

    @Test
    public void calculateNetSalary_grossAmountEqualToFixedCost() {
        result = salaryService.calculateNetSalary(200.0f, 200.0f, 0.5f);
        assertEquals(200.0f, result, DELTA);
    }

    @Test
    public void calculateNetSalary_grossAmountLowerThanFixedCost() {
        result = salaryService.calculateNetSalary(100.0f, 200.0f, 0.5f);
        assertEquals(100.0f, result, DELTA);
    }

    @Test
    public void calculateNetSalary_grossAmountDailyInfinite() {
        result = salaryService.calculateNetSalary(Float.POSITIVE_INFINITY, 200.0f, 0.5f);
        assertNull(result);
    }

}